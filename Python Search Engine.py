﻿
import os #file handling
import MySQLdb 
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import *
import gc

gc.enable()
word_list=[]
words = []
sublist=[]
word = []
filename = []

stemmer = PorterStemmer()

#connecting with MySQL
db = MySQLdb.connect("localhost","root","seecs@123","indexing" )  #connecting with MySQL using ID and Password and tablename 
cursor = db.cursor()    #cursor iterates over the returned result - initializing cursor

#Create Tables
#Forward Indexing Table
#sql = """CREATE  TABLE forwardindex (      
#         Doc_ID INT,
#         DOCNAME TEXT,
#         WORDS TEXT
         
#          )"""
#cursor.execute(sql) #execute query
###Inverted Indexing Table
#sql = """CREATE  TABLE invertedindex (
#         WORDID INT,
#         WORD TEXT,
#         DOCID TEXT,
#         FREQ INT
#          )"""
#cursor.execute(sql) #executes SQL query

#stopwords is a text corpora of NLTK Natural Language Tool Kit
stop_words = set(stopwords.words('english'))  #storing stop words of english language in stop_words

directory = os.path.normpath("C:/Users/Amal/Documents/DSA") #path of directory ; Normalizes the path by correcting the / , \ etc



gc.disable()    #garbage collector is disabled as appending to list becomes O(N) instead of O(1) as the size grows if gc is enabled.


#traversing through the directory O(n^2)

for subdir , dirs , files in os.walk(directory):      #walks through the directory
    for file in files:                                 #for every file in directory
                              #checks for text file
            f = open(os.path.join(subdir , file) , 'r')  #opens and reads the text file
            
            filename.append(file)                        #appending name of each file in filename list
            for line in f:
             
             words = line.strip()  ##removing commas , stops and hyphens frm the text ,parses a line of text 
            
            word.append(words) #appends the words of each file in word list
gc.enable()          #enable garbage collector
         
 
i=0
#removing stopwords O(n^2)
gc.disable()
while( i  < len(word)):                          #length of parsed list of lists
    j=0
    while( j < len(word[i])):                    #length of sublist of each file
        if word[i][j] not in stop_words:         #checking stop words
            sublist.append(word[i][j])           #sublist now contains non stopwords
        j+=1
    word_list.append(sublist)                    #sublist for each file is appended in word_list w.r.t document
                              
    sublist = []                                 #sublist is emptied before the loop runs for next file
    i+=1

gc.enable()

#inserting in MySQL  FORWARD INDEXING RELATION O(n):

i=0
while i < len(word_list):
 var=   ' '.join(word_list[i]) #coverting sublists of word_list into a string to insert in the database
 
 cursor.execute("INSERT INTO forwardindex VALUES (%s , %s , %s)" , ((i+1),filename[i],var)) #inserting docid , docname and text in each document
 db.commit()
 i+=1
stem_list = []
  
#stemming the document O(n):

x=0  
gc.disable() 
    
while x < len(word_list):
 stem = [stemmer.stem(word) for word in word_list[x]] #stem plurals to singulars and converting to first form
 stem_list.append(stem) #stemmmed words in stem_list
 x+=1

gc.enable()
#append stem_list into 'list' O(n^2)
a = 0
list=[]
gc.disable()
while a < (len(stem_list)): 
    b = 0
    while b < len(stem_list[a]):
        list.append(stem_list[a][b])    #accessing elements in sub_list  all words in a single list
        b+=1
    a+=1
gc.enable()
   
                                                     #redundancy check:
c=0
llist=[]
gc.disable()
for c in list:
    llist.append(c.lower())
unique = reduce(lambda l, x: l+[x] if x not in l else l, llist, [])   #unique now contains unique words from the texts add x if not already in l
gc.enable()
dlist = []
d=0

#insert into inverted indexing O(n^3)
while d < (len(unique)):
    e = 0
    while e < (len(stem_list)):
        counter=0
        f = 0
        while f < len(stem_list[e]):
            if unique[d] == (stem_list[e][f]).lower():          #converting in lower case and then comparing
               counter+=1
               cursor.execute("INSERT INTO invertedindex VALUES (%s , %s , %s, %s)" , ((d+1),unique[d],e+1,counter)) #inserting wordid words docid and frequency
               db.commit()
               
            f+=1
            
        e+=1
    
    
    dlist=[]
    d+=1

gc.enable()
                                                        #searching
query = raw_input("Enter the word you want to search ")
alt = """ALTER TABLE invertedindex 
            ADD FULLTEXT(WORD)"""      
cursor.execute(alt)
db.commit
query1 = stemmer.stem(query)
cursor.execute("""SELECT distinct DOCNAME AS id FROM forwardindex f JOIN invertedindex i ON (f.Doc_ID = i.DOCID)
 WHERE MATCH (WORD) AGAINST (%s in boolean mode)
 ORDER BY id DESC """ , [query1]  )
result = cursor.fetchall()
print result
